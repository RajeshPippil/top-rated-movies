package com.example.topratedmovies.adapter

import android.R.color
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.example.topratedmovies.R
import com.example.topratedmovies.model.TopRatedModel
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.card.MaterialCardView


class TopRatedAdapter(private val topRatedList: List<TopRatedModel>) :
    RecyclerView.Adapter<TopRatedAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.row_movie_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return topRatedList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Log.d("Response", "List Count :${topRatedList.size} ")
        return holder.bind(topRatedList[position], holder)

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var movieImage: AppCompatImageView = itemView.findViewById(R.id.movieImage)
        private var movieTitle: AppCompatTextView = itemView.findViewById(R.id.movieTitle)
        private var releaseDate: AppCompatTextView = itemView.findViewById(R.id.releaseDate)
        var cardView: MaterialCardView = itemView.findViewById(R.id.cardView)
        fun bind(topRated: TopRatedModel, holder: ViewHolder) {

            val circularProgressDrawable = CircularProgressDrawable(holder.itemView.context)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                circularProgressDrawable.colorFilter = BlendModeColorFilter(
                    holder.itemView.context.resources.getColor(
                        R.color.white,
                        null
                    ), BlendMode.SRC_ATOP
                )
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    circularProgressDrawable.setColorFilter(
                        holder.itemView.context.resources.getColor(
                            R.color.white,
                            null
                        ), PorterDuff.Mode.SRC_IN
                    )
                } else {
                    circularProgressDrawable.setColorFilter(
                        holder.itemView.context.resources.getColor(
                            R.color.white
                        ), PorterDuff.Mode.SRC_IN
                    )
                }
            }

            circularProgressDrawable.setColorFilter(
                holder.itemView.context.resources.getColor(
                    R.color.white,
                    null
                ), PorterDuff.Mode.SRC_IN
            )
            circularProgressDrawable.strokeWidth = 5f
            circularProgressDrawable.centerRadius = 30f
            circularProgressDrawable.start()

            movieTitle.text = topRated.title
            releaseDate.text = topRated.release_date
            val imageUrl = "https://image.tmdb.org/t/p/w500${topRated.poster_path}"

            Glide.with(holder.itemView.context)
                .load(imageUrl)
                .error(R.drawable.default_poster)
                .placeholder(circularProgressDrawable)
                .into(movieImage)

            cardView.setOnClickListener {
                val bt = BottomSheetDialog(holder.itemView.context)
                val view: View = LayoutInflater.from(holder.itemView.context)
                    .inflate(R.layout.bottom_sheet, null)
                val backdropImage: AppCompatImageView = view.findViewById(R.id.backdropImage)
                val movieTitleText: AppCompatTextView = view.findViewById(R.id.movieTitleText)
                val releaseDateText: AppCompatTextView = view.findViewById(R.id.releaseDateText)
                val ratingText: AppCompatTextView = view.findViewById(R.id.ratingText)
                val overviewText: AppCompatTextView = view.findViewById(R.id.overviewText)
                bt.setContentView(view);
                bt.show();

                val backdropUrl = "https://image.tmdb.org/t/p/w500${topRated.backdrop_path}"
                Glide.with(holder.itemView.context)
                    .load(backdropUrl)
                    .error(R.drawable.default_backdrop)
                    .placeholder(R.drawable.default_backdrop)
                    .into(backdropImage)

                val movieTitleStr: String = if (topRated.original_language == "en") {
                    topRated.title
                } else {
                    "${topRated.title} (${topRated.original_title})"
                }
                movieTitleText.text = movieTitleStr

                val date = "Release Date: ${topRated.release_date}"
                releaseDateText.text = date

                val rating = "Rating: ${topRated.vote_average}"
                ratingText.text = rating

                overviewText.text = topRated.overview
            }
        }
    }


}