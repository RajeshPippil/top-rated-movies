package com.example.topratedmovies

import androidx.lifecycle.*
import com.example.topratedmovies.model.TopRatedModel
import com.example.topratedmovies.repository.Repository
import com.google.gson.JsonObject
import kotlinx.coroutines.launch
import retrofit2.Response

class MainViewModel(private val repository: Repository) : ViewModel() {

    val allTopRated: LiveData<List<TopRatedModel>> = repository.allTopRated.asLiveData()

    val myResponse: MutableLiveData<Response<JsonObject>> = MutableLiveData()

    fun getTopRated() {
        val hashMap = LinkedHashMap<String, String>()
        hashMap["api_key"] = "1a4f4c49d8df132b510536bb6d59835d"
        hashMap["language"] = "en-US"
        hashMap["page"] = "1"

        viewModelScope.launch {
            val response: Response<JsonObject> = repository.getTopRated(hashMap)
            myResponse.value = response
        }
    }


}