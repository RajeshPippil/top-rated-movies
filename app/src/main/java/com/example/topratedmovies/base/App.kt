package com.example.topratedmovies.base

import android.app.Application
import com.example.topratedmovies.repository.Repository
import com.example.topratedmovies.room.TopRatedDatabase
import com.facebook.stetho.BuildConfig
import com.facebook.stetho.Stetho

class App : Application() {
    private var sApplication: Application? = null
    private val database by lazy { TopRatedDatabase.get(this) }
    val repository by lazy { Repository(database.getTopRatedDao()) }
    override fun onCreate() {
        super.onCreate()

        sApplication = this
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(sApplication)
        }

        TopRatedDatabase.get(sApplication!!)
    }
}