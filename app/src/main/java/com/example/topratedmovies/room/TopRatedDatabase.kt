package com.example.topratedmovies.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.topratedmovies.util.DataConverter
import com.example.topratedmovies.model.TopRatedModel

@Database(version = 1, entities = [TopRatedModel::class])
@TypeConverters(DataConverter::class)
abstract class TopRatedDatabase : RoomDatabase() {

    companion object {
        @Volatile
        private var INSTANCE: TopRatedDatabase? = null

        fun get(context: Context): TopRatedDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    TopRatedDatabase::class.java,
                    "top_rated_database"
                ).fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }

    abstract fun getTopRatedDao(): TopRatedDao
}