package com.example.topratedmovies.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.topratedmovies.model.TopRatedModel
import kotlinx.coroutines.flow.Flow

@Dao
interface TopRatedDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTopRated(list: List<TopRatedModel>)

    @Query("SELECT * FROM top_rated")
    fun getTopRated(): Flow<List<TopRatedModel>>
}