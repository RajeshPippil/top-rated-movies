package com.example.topratedmovies

import android.app.Application
import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.topratedmovies.adapter.TopRatedAdapter
import com.example.topratedmovies.base.App
import com.example.topratedmovies.model.TopRatedModel
import com.example.topratedmovies.room.TopRatedDatabase
import com.example.topratedmovies.util.NetworkConnection
import com.google.gson.Gson
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel
    private var topRatedModelList: List<TopRatedModel>? = null
    lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var emptyState: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val viewModelFactory = MainViewModelFactory(App().repository)
        viewModel = ViewModelProvider(this, viewModelFactory)[MainViewModel::class.java]

        emptyState = findViewById(R.id.emptyState)
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout)

        swipeRefreshLayout.post {
            fetchList()
            swipeRefreshLayout.isRefreshing =
                false
        }

        swipeRefreshLayout.setOnRefreshListener {
            fetchList()
            swipeRefreshLayout.isRefreshing =
                false
        }
    }

    private fun fetchList() {
        val networkConnection = NetworkConnection(this)
        networkConnection.observe(this) { isConnected ->
            if (isConnected) {
                emptyState.visibility = View.GONE
                viewModel.getTopRated()
                viewModel.myResponse.observe(this) { response ->
                    if (response.isSuccessful) {
                        val gson = Gson()
                        if (response.body() != null) {
                            try {
                                val stringJson = gson.toJson(response.body())
                                val `object` = JSONObject(stringJson)
                                val jsonArray = `object`.getJSONArray("results")

                                topRatedModelList = ArrayList()

                                for (i in 0 until jsonArray.length()) {
                                    val obj = jsonArray[i] as JSONObject
                                    val analyticsDataModel: TopRatedModel =
                                        gson.fromJson(obj.toString(), TopRatedModel::class.java)
                                    (topRatedModelList as ArrayList<TopRatedModel>).add(
                                        analyticsDataModel
                                    )
                                }
                                if (topRatedModelList != null)
                                    insertTopRatedData(topRatedModelList!!, application)

                            } catch (e: Exception) {
                                Log.d(TAG, "onCreate: ${e.message}")
                            }
                        }
                    } else {
                        emptyState.visibility = View.VISIBLE
                    }
                }
            } else {
                viewModel.allTopRated.observe(this) { topRated ->
                    if (topRated != null && topRated.isNotEmpty()) {
                        topRated.let {
                            emptyState.visibility = View.GONE
                            val topRatedRecycler: RecyclerView = findViewById(R.id.topRatedRecycler)
                            topRatedRecycler.apply {
                                setHasFixedSize(true)
                                layoutManager = GridLayoutManager(this@MainActivity, 2)
                                // layoutManager = LinearLayoutManager(this@MainActivity)
                                adapter = TopRatedAdapter(it)
                            }
                        }
                    } else {
                        emptyState.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private fun insertTopRatedData(list: List<TopRatedModel>, application: Application) {
        Thread {
            TopRatedDatabase.get(application).getTopRatedDao().insertTopRated(list)

            runOnUiThread {
                viewModel.allTopRated.observe(this) { topRated ->
                    topRated.let {
                        val topRatedRecycler: RecyclerView = findViewById(R.id.topRatedRecycler)
                        topRatedRecycler.apply {
                            setHasFixedSize(true)
                            layoutManager = GridLayoutManager(this@MainActivity, 2)
                            // layoutManager = LinearLayoutManager(this@MainActivity)
                            adapter = TopRatedAdapter(it)
                        }
                    }
                }
            }

        }.start()
    }
}