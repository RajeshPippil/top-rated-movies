package com.example.topratedmovies.api

import com.example.topratedmovies.util.Constants.Companion.BASE_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {

    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val api: NetworkServices by lazy {
        retrofit.create(NetworkServices::class.java)
    }
}