package com.example.topratedmovies.api

import com.google.gson.JsonObject
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.QueryMap
import java.util.LinkedHashMap

interface NetworkServices {

    @GET("movie/top_rated")
    suspend fun getTopRated(@QueryMap hashMap: LinkedHashMap<String, String>): Response<JsonObject>
}