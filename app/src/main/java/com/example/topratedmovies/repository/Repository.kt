package com.example.topratedmovies.repository

import com.example.topratedmovies.api.RetrofitInstance
import com.example.topratedmovies.model.TopRatedModel
import com.example.topratedmovies.room.TopRatedDao
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import retrofit2.Response

class Repository(private val topRatedDao: TopRatedDao) {

    val allTopRated: Flow<List<TopRatedModel>> = topRatedDao.getTopRated()

    suspend fun getTopRated(hashMap: LinkedHashMap<String, String>): Response<JsonObject> {
        return RetrofitInstance.api.getTopRated(hashMap)
    }

}